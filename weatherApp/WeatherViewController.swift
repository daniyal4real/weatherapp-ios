//
//  ViewController.swift
//  weatherApp
//
//  Created by Daniyal on 1/22/21.
//  Copyright © 2021 Daniyal. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class WeatherViewController: UIViewController, CLLocationManagerDelegate, GetWeatherViewControllerDelegate{

    
    @IBOutlet weak var convertTempSwitch: UISwitch!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherConditionImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    public let WEATHER_URL: String = "https://api.openweathermap.org/data/2.5/weather"
    public let API_KEY: String = "8b9f776f695b68ae7da0bef7865b0554"
    
    // GLOBAL VARIABLE TEMPRETURE
    public var tempreture: Double = 0
   
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            print("Location: \(location)")
            let params: [String: Any] = ["lat": location.coordinate.latitude, "lon": location.coordinate.longitude, "appid": API_KEY, "units" : "metric"]
            getWeatherData(url: WEATHER_URL, params: params)
        }
    }
    
    func getWeatherData(url: String, params: [String: Any]){
        AF.request(url, method: .get, parameters: params).responseJSON { (response) in
            switch response.result {
            case .success( _ ):
                do {
                    if let resposeData = response.data {
                        print(response)
                        let json = try JSONDecoder().decode(WeatherEntity.self, from: resposeData)
                        // SAVED THE TEMPRETURE TO THE GLOBAR VARIABLE
                        self.tempreture = json.main.temp
                        self.updateUI(json: json)
                    }else {
                        print("Failed to unwrap")
                    }
                } catch {
                    print(error)
                    self.cityNameLabel.text = "Connection failure"
                }
            case .failure(let error):
                print(error)
                self.cityNameLabel.text = "Location unavailable"
            }
        }
    }
    
    func updateUI(json: WeatherEntity) {
        temperatureLabel.text = "\(Int(json.main.temp))℃"
        weatherConditionImageView.image = UIImage(named: updateWeatherIcon(condition: json.weather.first?.id ?? -1))
        cityNameLabel.text = json.name
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error")
        cityNameLabel.text = "Location unavailable"
    }
    
    func updateWeatherIcon(condition: Int) -> String {
    switch (condition) {
    case 0...300 :
        return "tstorm1"
    case 301...500 :
        return "light_rain"
    case 501...600 :
        return "shower3"
    case 601...700 :
        return "snow4"
    case 701...771 :
        return "fog"
    case 772...799 :
        return "tstorm3"
    case 800 :
        return "sunny"
    case 801...804 :
        return "cloudy2"
    case 900...903, 905...1000 :
        return "tstorm3"
    case 903 :
        return "snow5"
    case 904 :
        return "sunny"
    default :
        return "dunno"
        
    }
}
    
    func getWeatherForCity(with name: String) {
        let params: [String: Any] = ["q" : name, "appid" : API_KEY, "units" : "metric"]
        getWeatherData(url: WEATHER_URL, params: params)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "city" {
            if let destination = segue.destination as? GetWeatherViewController {
                destination.delegate = self
            }
        }
    }
    
    @IBAction func convertTempPressed(_ sender: UISwitch) {
        if sender.isOn == true {
            // IF THE SWITCH IS PRESSED, THEN SHOW THE TEMPRETURE IN CELCIOUS FROM GLOBAL VARIABLE
            temperatureLabel.text = "\(Int(tempreture))℃"
        }else{
            // IS THE SWITCH IS NOT PRESSED, THEN PUT THE TEMPRETURE VALUE TO FORMULA AND ASSIGNED THE RESULT TO TEMP VARIABLE
            // THEN DISPLAYED THE TEMPRETURE IN FAHRENHEIT
            let temp = (tempreture * 9/5) + 32
            temperatureLabel.text = "\(Int(temp))℉"
        }
    }
    
    @IBAction func openGetWeatherVC(_ sender: Any) {
    }
}

